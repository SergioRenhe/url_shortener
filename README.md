# Django URL Shortener

The Django URL Shortener is a web application that allows users to create short URLs for publishing promotions or sharing links. It provides a RESTful API for managing URLs and resolving short URLs to their corresponding long URLs. The project uses Django and Django REST Framework for building the web application.

## Features

- Create short URLs for long URLs
- Modify destination URL for existing short URLs
- Disable or enable short URLs
- Resolve short URLs to their corresponding long URLs
- Access nearly real-time usage statistics #TODO

## Serverless Infrastructure with Zappa

The Django URL Shortener is designed to run on a serverless infrastructure using Zappa, which allows you to deploy Django applications to serverless platforms such as AWS Lambda. Running the application in a serverless environment provides benefits such as scalability, cost-efficiency, and easy deployment.

## Requirements

All requirements are listed in the pyproject.toml file

## Architecture Diagram

There are diagrams in the diagrams/ folder that illustrates the components and flow of the Django URL Shortener application. The proposed architecture should have a theoretical availability of 5 nines (one more nine than asked in the requisites).

## Usage

- Create a short URL:
  - Make a POST request to `/urls/` with the `long_url` parameter in the request body.

- Modify the destination URL:
  - Make a PATCH request to `/urls/<short_url>/` with the `long_url` parameter in the request body.

- Disable a short URL:
  - Make a PATCH request to `/urls/<short_url>/` with the `is_enabled` parameter set to `false` in the request body.

- Enable a short URL:
  - Make a PATCH request to `/urls/<short_url>/` with the `is_enabled` parameter set to `true` in the request body.

- Resolve a short URL:
  - Access the short URL in your browser or make a GET request to `http://localhost:8000/<short_url>/`.

## TODO List

- Reconfigure the application to use DynamoDB as the database for storing URLs.
- Add a Zappa configuration file to specify the serverless deployment settings.
- Perform load tests to ensure the application can handle 5000 RPM and 1M traffic peak.
- Add a Terraform file to better describe the infrastructure needed for real-time data analytics.
- Develop a Single Page Application (SPA) for end users to easily create their short URLs.
- Implement authentication, authorization, and security measures such as throttling/rate limits.
- Reduce the theoretical availability to 4 nines (99.99%) as requested, to avoid overengineering and excessive costs.
- Deploy to production.


