from rest_framework import viewsets
from django.shortcuts import redirect
from django.http import HttpResponseBadRequest
from url_shortener.models import URL
from url_shortener.serializers import URLSerializer


def resolve_url(request, short_url):
    try:
        url = URL.objects.get(short_url=short_url)
        if url.is_enabled:
            # Update click statistics, integrate with data analytics or perform any additional logic here
            return redirect(url.long_url)
        else:
            return HttpResponseBadRequest("This URL is disabled.")
    except URL.DoesNotExist:
        return redirect('/404/')


class URLViewSet(viewsets.ModelViewSet):
    queryset = URL.objects.all()
    serializer_class = URLSerializer
